# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-10 12:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0017_server_protocol'),
    ]

    operations = [
        migrations.RenameField(
            model_name='server',
            old_name='wprocess',
            new_name='process',
        ),
        migrations.RenameField(
            model_name='server',
            old_name='wservice',
            new_name='service',
        ),
    ]

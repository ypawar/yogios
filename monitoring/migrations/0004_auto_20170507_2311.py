# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-07 17:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitoring', '0003_auto_20170507_2309'),
    ]

    operations = [
        migrations.RenameField(
            model_name='server',
            old_name='ip',
            new_name='IP',
        ),
    ]

var table = $('#example').DataTable( {
        "ajax": "/yogios/getconfiguredservers",
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<a href='#' class='btn btn-sm'><span class='glyphicon glyphicon-pencil'></span</a>\
            <a href='#' class='btn btn-sm'><span class='glyphicon glyphicon-remove'></span></a>"
        },
        {"visible": false, "targets": 0},
        {"language": {"emptyTable": "No Server(s) Configured to show!"}},

         ]
    } );

//Deactivate server
$('#example tbody').on( 'click', '.glyphicon-remove', function () {
        trow = table.row( $(this).parents('tr') )
        var rdata = trow.data();
//        alert( "Remove ID is :" + data[0] );
        $.get("/ajax/deleteserver/",{"id":rdata[0]},function(data,status){

        if (data=="OK")
        {
            console.log(data)
            console.log("Deleting Row")
            trow.remove().draw();
            console.log("Updated  Table")
        }
        })
    } );

$('#example tbody').on( 'click', '.glyphicon-pencil', function () {
        trow = table.row( $(this).parents('tr') )
        var rdata = trow.data();
//        alert( "Remove ID is :" + data[0] );
        document.location.href = "/yogios/configs/" + rdata[0];

    } );
//Hide the refresh glyphicon
$('#refresh').hide()
$("#servertest").click(function()
{
    console.log("Test Clicked");
    $('div.modal-body').html(function()
        {return '<span class="glyphicon glyphicon-refresh"></span> Processing'
        ;})
    $("#myModal").modal()
    var ip = $('#id_ip').val()
    var wusername = $('#id_wuser').val()
    var wpassword = $('#id_wpswd').val()
    var suser = $('#id_suser').val()
    var spswd = $('#id_spswd').val()
    var sport = $('#id_sport').val()
    $.ajax
    ({
        url: '/ajax/validate_server/',
        data: {
           'ip': ip,
          'wusername': wusername,
          'wpassword': wpassword,
          'suser': suser,
          'spswd': spswd,
          'sport': sport
        },
        dataType: 'json',
        success: function (data) {
            /*
            alert("OS:\t\t"+data.os+'\n'
            +"CPU:\t"+data.name+'\n'+
            "Socket:\t"+data.socket+'\n'+
            "Manufacturer:\t"+data.manufacturer+'\n'+
            "Product:\t"+data.product+'\n'+
            "SN:\t"+data.SN+'\n'
            );
            */

            var op = "Hostname:\t"+data.hostname+'<br>'+
            "OS:\t\t"+data.os+'<br>'+
            "Uptime:\t"+data.uptime+'<br>'+
            "CPU:\t"+data.name+'<br>'+
//            "Socket:\t"+data.socket+'<br>'+
            "Manufacturer:\t"+data.manufacturer+'<br>'+
            "Product:\t"+data.product+'<br>'+
//            "SN:\t"+data.SN+'<br>'+
            "RAM:\t"+data.ram

            $('#id_os').val(data.os)
            $('#id_cpu').val(data.name)
            $('#id_board').val(data.manufacturer)
            $('#id_product').val(data.product)
//            $('#id_socket').val(data.socket)
            $('#id_ram').val(data.ram)
            $('#id_hostname').val(data.hostname)

            $('div.modal-body').html(function()
            {return op;}
            )
        }
    });
});


$('#getserverdata').click(function()
{

    $('#id_service').empty()
    $('#id_process').empty()
    $('#id_drive').empty()
    console.log('Getting Server Data');
    var i = 0;
    var arr = {};
    var proceed = false;
    var monitor = $('#id_monitor').val();
    if (monitor.length > 0)
    {
        proceed = true;
    }
//       $("[name='checkboxes']").each(function ()
//       {
//
//           arr[this.value]=this.checked;
//           if (this.checked)
//           {
//            proceed=true;
//           }
//
//       });


    var ip = $('#id_ip').val()
    var wusername = $('#id_wuser').val()
    var wpassword = $('#id_wpswd').val()
    var suser = $('#id_suser').val()
    var spswd = $('#id_spswd').val()
    var sport = $('#id_sport').val()
    arr['ip'] = ip
    arr['wusername'] = wusername
    arr['wpassword'] = wpassword
    arr['suser'] = suser
    arr['spswd'] = spswd
    arr['sport'] = sport
    arr['monitor'] = monitor
    console.log(arr);
    if (proceed)
    {
        $('#refresh').show()
        $.ajax(
        {
        url: '/ajax/get_server_data/',
            data: arr,
            dataType: 'json',
            success: function (data)
            {
                console.log(data)

                if (data.services)
                {
                    var options=[];
                    for ( var i=0;i<data.services.length;i++)
                    {
                        $('#id_service').append("<option>"+data.services[i] +"</option>")
//                        options[i] = {label:data.services[i],title:data.services[i],value:data.services[i]}
                    }
//                  $('#services').multiselect('dataprovider',options)
//                  $('#services').multiselect('enable')
                    $('#id_service').selectpicker('refresh')
                    $('.dropdown-toggle').each(function(index)
                    {
                        if ($(this).data('id')=='id_service')
                        {
                            $(this).attr('disabled',false)
                        }
                    })

                }

                if (data.processes)
                {

                    for ( var i=0;i<data.processes.length;i++)
                    {
                        $('#id_process').append("<option>"+data.processes[i] +"</option>")
                    }
                    $('#id_process').selectpicker('refresh')
                    $('.dropdown-toggle').each(function(index)
                    {
                        if ($(this).data('id')=='id_process')
                        {
                            $(this).attr('disabled',false)
                        }
                    })
                }
                $('#id_drive').selectpicker('refresh')
                if (data.drives)
                {

                    for ( var i=0;i<data.drives.length;i++)
                    {
                        protocol = $('#id_protocol').val()
                        console.log(protocol)
                        if (protocol == 'wmi')
                        {
                            v = data.drives[i].split(':')[0]
                            $('#id_drive').append('<option value="' + v + '">' + data.drives[i] + '</option>')
//                            $('#drives').selectpicker({header:"Drive Used"})
//                            $('#drives').prop("data-header","Drive\tUsed")
                        }
                        else if (protocol == 'ssh')
                        {

                            v = data.drives[i].split(' ')
                            $('#id_drive').append('<option value="' + v[v.length-1] + '">' + data.drives[i] + '</option>')
//                            $('#drives').selectpicker({header:"Device Used Mount Point"})
//                            $('#drives').prop("data-header","Drive\tUsed\tMount Point")
                        }
                    }
                    $('#id_drive').selectpicker('refresh')
                    $('.dropdown-toggle').each(function(index)
                    {
                        if ($(this).data('id')=='id_drive')
                        {
                            $(this).attr('disabled',false)
                        }
                    })
                }
                $('#refresh').hide()
            }
        });

    }
});

//$('#services').multiselect({enableFiltering:true,nonSelectedText :'Select Service(s)',disableIfEmpty:true,enableCaseInsensitiveFiltering:true,maxHeight:300});
$('#id_protocol').selectpicker({liveSearch:false,showTick:true});
//$('#id_protocol').attr('width',"fit")
$('#id_service').selectpicker({liveSearch:true});
$('#id_process').selectpicker({liveSearch:true});
$('#id_drive').selectpicker({liveSearch:false});
$('#id_monitor').attr('multiple','multiple');
$('#id_monitor').selectpicker({liveSearch:false});

//$('.dropdown-toggle').attr('disabled',true);
//Disable All select boxes except protocol
$('.dropdown-toggle').each(function(index)
    {
//        if ($(this).data('id')!='id_protocol') ($(this).data('id')!='id_monitor'))
//          $(this).attr('disabled',true);
//        }
//        if ($(this).data('id')!='id_protocol') ($(this).data('id')!='id_monitor'))
//          $(this).attr('disabled',true);
        }
    );



function showWMI()
{
    hideSSH()
    hideSNMP()
    $("#id_wuser").prop('required',true);
    $("#id_wpswd").prop('required',true);
    $('#id_div_wuser').show()
    $('#id_div_wpswd').show()
}

function showSSH()
{
    hideWMI()
    hideSNMP()
    $("#id_suser").prop('required',true);
    $("#id_spswd").prop('required',true);
    $("#id_sport").prop('required',true);
    $('#id_div_suser').show()
    $('#id_div_spswd').show()
    $('#id_div_sport').show()
    $('#id_div_skey').show()
}

function showSNMP()
{
    hideWMI()
    hideSSH()
    $("#id_cstrng").prop('required',true);
    $('#id_div_cstrng').show()
}

function hideWMI()
{
    $('#id_div_wuser').hide()
    $('#id_div_wpswd').hide()
    $("#id_wuser").prop('required',false);
    $("#id_wpswd").prop('required',false);
}

function hideSSH()
{
    $('#id_div_suser').hide()
    $('#id_div_spswd').hide()
    $('#id_div_sport').hide()
    $('#id_div_skey').hide()
    $("#id_suser").prop('required',false);
    $("#id_spswd").prop('required',false);
    $("#id_sport").prop('required',false);
}

function hideSNMP()
{
    $('#id_div_cstrng').hide()
    $("#id_cstrng").prop('required',false);
}

hideSSH()
hideSNMP()
//hideWMI()

function showService()
{
    $('#id_div_service').show()
}

function showProcess()
{
    $('#id_div_process').show()
}

function showDrive()
{
    $('#id_div_drive').show()
}

function hideService()
{
    $('#id_div_service').hide()
}

function hideProcess()
{
    $('#id_div_process').hide()
}

function hideDrive()
{
    $('#id_div_drive').hide()
}

function showCredentials(protocol)
{
    if (protocol=='wmi')
    {
//    console.log("Show WMI")
        showWMI()
    }
    if (protocol=='ssh')
    {
        showSSH()
    }
    if (protocol=='snmp')
    {
        showSNMP()
    }
}

function showMonitoring(monitoring)
{
    if ($.inArray("process",monitoring)>=0)
    {
        showProcess();
    }
    else
    {
        $('#id_process').empty();
        $('#id_process').selectpicker('refresh');
        hideProcess();
    }
   if ($.inArray("service",monitoring)>=0)
    {
        showService();
    }
    else
    {
        $('#id_service').empty();
        $('#id_service').selectpicker('refresh');
        hideService();
    }
   if ($.inArray("drive",monitoring)>=0)
    {
        showDrive();
    }
    else
    {
        $('#id_drive').empty();
        $('#id_drive').selectpicker('refresh');
        hideDrive();
    }
}
$('#id_protocol').on('changed.bs.select', function (e) {
//  var selectedD = $(this).find('option').eq(clickedIndex).text();
//    console.log('selectedD: ' + selectedD + '  newValue: ' + newValue + ' oldValue: ' + oldValue);
    selected = $(e.currentTarget).val();
//    console.log(selected);
    showCredentials(selected);

});

hideDrive()
hideProcess()
hideService()

$('#id_monitor').on('changed.bs.select', function (e) {
    selected = $(e.currentTarget).val();
    console.log(selected);
    showMonitoring(selected);

});

$(document).ready(function()
{
    showCredentials($('#id_protocol').val());
    showMonitoring($('#id_monitor').val());
});
from django import forms
from models import Server


class ServerForm(forms.ModelForm):
    # wpswd = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Server
        fields = ('name',
                  'ip',
                  'protocol',
                  'wuser',
                  'wpswd',
                  'suser',
                  'spswd',
                  'skey',
                  'sport',
                  'cstrng',
                  'monitor',
                  'service',
                  'process',
                  'drive',
                  # 'sservice',
                  # 'sprocess',
                  'hostname',
                  'os',
                  'board',
                  'product',
                  'cpu',
                  'ram',
                  # 'socket'
                  )

        # labels = {'ip':'IP'}
        widgets={'wpswd':forms.PasswordInput(render_value=True)}
    def __init__(self, *args, **kwargs):
        super(ServerForm, self).__init__(*args, **kwargs)
        self.fields['hostname'].required = False
        self.fields['wuser'].required = False
        self.fields['wpswd'].required = False
        self.fields['suser'].required = False
        self.fields['spswd'].required = False
        self.fields['skey'].required = False
        self.fields['sport'].required = False
        self.fields['cstrng'].required = False
        self.fields['monitor'].required = False
        self.fields['service'].required = False
        self.fields['process'].required = False
        self.fields['drive'].required = False
        # self.fields['sservice'].required = False
        # self.fields['sprocess'].required = False
        self.fields['os'].required = False
        self.fields['board'].required = False
        self.fields['product'].required = False
        self.fields['cpu'].required = False
        self.fields['ram'].required = False

        # self.fields['socket'].required = False



# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import HttpResponse,render,HttpResponseRedirect,redirect
from django.http import JsonResponse
from django.shortcuts import render
from django.contrib import auth
from forms import ServerForm
# import wmi
from django.conf import settings
from wmicommands import ExecuteWMI as WMI
from linuxcommands import SSH
from models import Server
import pythoncom
import socket
from django.core import serializers
# Create your views here.
# def index(request):
#     return HttpResponse('Just Home Page!')

# def home(request):
#     return HttpResponse('YogiOS Home Page!')


def login(request):
    name = 'Yogesh Pawar'
    args = {'name':name,'title':'Login Page'}
    request.session.set_expiry(0)
    return render(request,'yogios/login.html',args)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/yogios')


def configserver(request, server_id=''):
    if request.method == 'GET':
        if server_id != '' :
            form = ServerForm(instance=Server.objects.get(pk=server_id))
        else:
            form = ServerForm()
        # form =ServerForm(instance=Server.objects.last())
        # ServerForm(instance=Server.objects.last())
    # return HttpResponse('Server Configuration Form')
    #     return render(request,'yogios/configs.html',{'form': form,'method': 'GET'})
        return render(request, 'yogios/serverconfig.html', {'form': form, 'method': 'GET'})
    else:
        print request.POST

        data = {'name': request.POST.get('name'),
                'ip': request.POST.get('ip'),
                'hostname': request.POST.get('hostname'),
                'protocol': request.POST.get('protocol'),
                'wuser': request.POST.get('wuser'),
                'wpswd': request.POST.get('wpswd'),
                'suser': request.POST.get('suser'),
                'spswd': request.POST.get('spswd'),
                'skey': request.POST.get('skey'),
                'sport': request.POST.get('sport'),
                'cstrng': request.POST.get('cstrng'),
                'monitor':','.join(request.POST.getlist('monitor')),
                'service': ','.join(request.POST.getlist('service')),
                'process': ','.join(request.POST.getlist('process')),
                'drive': ','.join(request.POST.getlist('drive')),
                'os': request.POST.get('os'),
                'board': request.POST.get('board'),
                'product': request.POST.get('product'),
                'cpu': request.POST.get('cpu'),
                'ram': request.POST.get('ram'),
                'csrfmiddlewaretoken': request.POST.get('csrfmiddlewaretoken')
                }

        if server_id:
            form = ServerForm(data,instance=Server.objects.get(pk=server_id))
        else:
            form = ServerForm(data)
        print "data",data

        if form.is_valid():
            form.save(commit=True)
            print 'Form saved'
            return render(request,'yogios/configs.html', {'method': 'POST'})
        else:
            print "Errors"
            print form.errors
            return HttpResponse("Values Incorrect")
        # else:
        #     sform = ServerForm(instance=form)
        #     return render(request, 'yogios/configs.html', {'form': sform, 'method': 'GET'})


def discovery(request):
    return HttpResponse('Auto Discovery Menu')


def validate_server(request):
    if request.user.is_authenticated:
        uname = request.GET.get('wusername',None)
        protocol = 'w'
        if uname == "":
            uname = request.GET.get('suser', None)
            pswd = request.GET.get('spswd', None)
            port = request.GET.get('sport', None)
            protocol = 's'
        else:
            pswd = request.GET.get('wpassword', None)
        ip = request.GET.get('ip', None)
        response={}
        print "Protocol",protocol
        if protocol=='w':
            pythoncom.CoInitialize()
            # if settings.DEBUG:
            selfip = getselfip()
            print "selfip", selfip
            if selfip == ip:
                con = WMI()
            else:
                con = WMI(ip,uname,pswd)
            print "Connected WMI"
        elif protocol=='s':
            print pswd
            con = SSH(ip=ip,usr=uname,pswd=pswd)
            print "Connected SSH"
        # con = wmi.WMI()
        # for os in con.Win32_OperatingSystem():
        #     response['os'] = os.Caption.strip()
        # for cpu in con.Win32_Processor():
        #     response['name'] = cpu.Name
        #     response['socket'] =  cpu.SocketDesignation
        # for mbrd in con.Win32_BaseBoard():
        #     response ['manufacturer'] =  mbrd.Manufacturer
        #     response ['product'] =  mbrd.Product
        #     response ['SN']  = mbrd.SerialNumber
        # for mem in con.Win32_PhysicalMemory():
        #     print float(mem.Capacity)/(2**30),'GB',mem.Manufacturer, mem.PartNumber, mem.Speed
        #     response['ram'] = ' '.join([str(float(mem.Capacity)/(2**30)),'GB',mem.Manufacturer,'@',str(mem.Speed),'MHz', mem.PartNumber])
        response['os'] = con.getos()
        cpu = con.getcpu()
        response['name'] = cpu['name']
        # response['socket'] =  cpu['socket']
        board = con.getboard()
        response['manufacturer'] = board['manufacturer']
        response['product'] =  board['product']
        # response['SN']  = board['SN']
        response['ram'] = con.getmemory() + ' MB'
        response['hostname'] = con.gethostname()
        response['uptime'] = str(con.getuptime()['utime'])
        return JsonResponse(response)
    else:
        return HttpResponse(
            'Unauthorised Access!'
        )


def getselfip():
    return ([l for l in (
        [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [
            [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in
             [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])


def getserverdata(request):
    if request.user.is_authenticated:
        if request.method=='GET':
            print request.GET
            uname = request.GET.get('wusername', None)
            protocol = 'w'
            if uname == "":
                uname = request.GET.get('suser', None)
                pswd = request.GET.get('spswd', None)
                port = request.GET.get('sport', None)
                protocol = 's'
            else:
                pswd = request.GET.get('wpassword', None)
            ip = request.GET.get('ip', None)
            response = {}
            print "Protocol", protocol
            if protocol == 'w':
                pythoncom.CoInitialize()
                # if settings.DEBUG:
                selfip = getselfip()
                print "selfip", selfip
                if selfip == ip:
                    con = WMI()
                else:
                    con = WMI(ip, uname, pswd)
                print "Connected WMI"
            elif protocol == 's':
                # print pswd
                con = SSH(ip=ip, usr=uname, pswd=pswd)
                print "Connected SSH"

            monitor = request.GET.getlist('monitor[]')
            print "monitor", monitor
            if 'process' in monitor:
                # processes=[]
                # for process in con.Win32_Process():
                #     processes.append(process.Caption)
                response['processes'] = con.getprocesses()

            if 'service' in monitor:
                # services = []
                # for service in con.Win32_Service():
                #     services.append(service.Name)
                response['services'] = con.getservices()

            if 'drive' in monitor:
                # disks = []
                # for drive in con.Win32_LogicalDisk():
                #     # if drive.DriveType==3:
                #     if drive.Size:
                #         size = float(drive.Size)
                #         free = float(drive.FreeSpace)
                #         used = (size - free) / size * 100.
                #         used = "{} {:.2f}% Used".format(drive.DeviceID, used)
                #         disks.append(used)
                # print disks
                response['drives'] = con.getdrives()
            # print response
            return JsonResponse(response)


def viewserver(request):
    if request.method=="GET":
        return render(request,'yogios/viewserver.html')


def getconfiguredservers(request):
    if request.user.is_authenticated:
        if request.method=="GET":
            servers = Server.objects.values_list('id', 'ip', 'name', 'hostname').filter(active=True)
            response = {'data': list(servers)}
        return JsonResponse(response,safe=True)
    else:
        return HttpResponse(
            'Unauthorised Access!'
        )

def deleteserver(request):
    if request.user.is_authenticated:
        if request.method == "GET":
            id = request.GET.get('id')
            Server.objects.filter(pk=id).update(active=False)
            return HttpResponse("OK")

# def configureserver(request):
#     if request.method=="GET":
#         form = ServerForm(instance=Server.objects.last())
#         print list(form)
#         selfip = getselfip()
#         print selfip
#         return render(request,"yogios/configureserver.html",{'form': form})



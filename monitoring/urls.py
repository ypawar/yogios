from django.conf.urls import url
from . import views
from django.contrib.auth.views import login
urlpatterns =[
    url(r'^$',login,{'template_name':'yogios/login.html'}),
    # url(r'^login$',views.login),
    # url(r'^login$',login,{'template_name':'yogios/login.html'}),
    url(r'^logout/$',views.logout),
    url(r'^configs/(?P<server_id>\d+)/$',views.configserver),
    url(r'^configs/$',views.configserver),
    url(r'^viewserver/$',views.viewserver),
    url(r'^getconfiguredservers/$',views.getconfiguredservers),
    url(r'^discovery/$',views.discovery),
    url(r'^validate_server/$',views.validate_server),
    url(r'^get_server_data/$',views.getserverdata),
    url(r'^deleteserver/$',views.deleteserver),
    # url(r'^configureserver/$',views.configureserver),
]
import spur
from  datetime import datetime,timedelta

class SSH:
    def __init__(self, ip, usr, pswd=None, key=None, port=22):
        self.ip = ip
        self.usr = usr
        self.pswd = pswd
        self.key = key
        self.port = port

    def init(self):
        if self.key is not None:
            self.shell = spur.SshShell(hostname=self.ip, port=self.port, username=self.usr, private_key_file=self.key,
                                       connect_timeout=60,
                                       shell_type=spur.ssh.ShellTypes.minimal,
                                       missing_host_key=spur.ssh.MissingHostKey.accept)
            print "SSH Connected"

        else:
            self.shell = spur.SshShell(hostname=self.ip, port=self.port, username=self.usr, password=self.pswd,
                                       connect_timeout=60,
                                       shell_type=spur.ssh.ShellTypes.minimal,
                                       missing_host_key=spur.ssh.MissingHostKey.accept)
            print "SSH Connected"

    def getos(self):
        pass
        self.init()
        with self.shell:
            result = self.shell.run(["uname", "-a"]).output
            return result

    def getcpu(self):
        self.init()
        with self.shell:
            result = self.shell.run(["lscpu"]).output
            cpuinfo = {}
            search = ['Model name', 'CPU(s)', 'CPU MHz']
            for line in result.split('\n'):
                l = line.split(':')
                key = l[0]
                if key in search:
                    if key == 'Model name':
                        key = 'name'
                    # if key == 'CPU(s)':
                    #     key = 'cores'
                    # if key == 'CPU MHz':
                    #     key = 'speed'
                    value = l[1].strip()
                    cpuinfo[key] = value
        return cpuinfo

    def getmemory(self):
        pass
        # TODO (Add Swap Support)
        self.init()
        with self.shell:
            result = self.shell.run(["cat", "/proc/meminfo"]).output
            for line in result.split('\n'):
                l = line.split(':')
                if l[0] == 'MemTotal':
                    return str(int(l[1].strip().split(' ')[0])/1024)

    def getboard(self):
        pass
        self.init()
        with self.shell:
            manufacturer = self.shell.run(["cat", "/sys/devices/virtual/dmi/id/board_vendor"]).output.strip()
            product = self.shell.run(["cat", "/sys/devices/virtual/dmi/id/board_name"]).output.strip()
            return {'manufacturer': manufacturer,'product': product}

    def getdrives(self):
        pass
        self.init()
        with self.shell:
            result = self.shell.run(["df", "-m", "-x", "tmpfs", "-x", "devtmpfs", "--output=source,pcent,target"]).output
            # print result
            lines = result.split('\n')
            drives=[]
            # print lines
            for line in lines[1:-1]:
                drives.append(line.strip())
            # Exclude first tag line
            return drives

    def getuptime(self):
        self.init()
        with self.shell:
            result = self.shell.run(["cat", "/proc/uptime"]).output
            seconds = int(result.split(' ')[0].split('.')[0])
            ut = timedelta(seconds=seconds)
            result = self.shell.run(["date", '+%Y%m%d%H%M%S']).output
            ct = result.strip()
            ct = datetime.strptime(ct, '%Y%m%d%H%M%S')
            bt = ct-ut
        return {'btime': bt, 'ctime': ct, 'utime': ut}

    def getservices(self):
        pass
        self.init()
        with self.shell:
            result = self.shell.run(["systemctl", "list-unit-files", "--type=service", "--plain"]).output
        services = []
        for line in result.split('\n'):
            # Ignore blank lines
            if line:
                s = line.strip().split()[0]
                if s.endswith('.service'):
                    # Exclude service name ending with '@' as it doesn't provide status information
                    if s[-9] != '@':
                        # print i,s[:-8]
                        services.append(s[:-8])
        services.sort()
        return services

    def getprocesses(self):
        pass
        self.init()
        with self.shell:
            result = self.shell.run(["ps", "-A", "-o", "comm"]).output
            # print result
        lines = result.split('\n')
        processes=[]
        for line in lines[1:]:
            process = line.strip()
            # Don't include duplicate process
            if (process not in processes) and (process not in ["ps",""]):
                # Don't include our commands itself
                processes.append(process)
        # print processes
        processes.sort()
        return processes


    def gethostname(self):
        pass
        self.init()
        with self.shell:
            result = self.shell.run(["hostname"]).output.strip()
            return result

    def getmemorystat(self):
        self.init()
        with self.shell:
            result = self.shell.run(["cat", "/proc/meminfo"]).output
            mem = {}
            search = ['MemTotal', 'MemFree', 'SwapTotal', 'SwapFree']
            for line in result.split('\n'):
                l = line.split(':')
                key = l[0]
                if key in search:
                    value = l[1].strip().split(' ')[0]
                    # Value in MB
                    mem[key] = int(value) / 1024

        return mem

    def getcpustat(self):
        self.init()
        with self.shell:
            result = self.shell.run(["cat", "/proc/loadavg"]).output
            # Divide by core count
            la = float(result.split(' ')[0]) / 2
            if la > 1:
                la = 1
            return la * 100

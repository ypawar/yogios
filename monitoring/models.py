# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Server(models.Model):
    protocols = [('wmi', 'WMI'),
                 ('ssh', 'SSH'),
                 ('snmp', 'SNMP')]
    monitoring = [('service', 'Service'),
                 ('process', 'Process'),
                 ('drive', 'Drive')]
    name = models.CharField(max_length=60,null=False,verbose_name='Name/Alias')
    ip = models.GenericIPAddressField(blank=False,null=False,verbose_name="IP")
    hostname = models.CharField(max_length=50,null=True,blank=True)
    protocol = models.CharField(max_length=4,null=False,blank=False,default='wmi',choices=protocols)
    wuser = models.CharField(max_length=50,null=True,blank=True,verbose_name='WMI Username')
    wpswd = models.CharField(max_length=30,null=True,blank=True,verbose_name='WMI Password')
    suser = models.CharField(max_length=50, null=True,blank=True,verbose_name='SSH Username')
    spswd = models.CharField(max_length=30, null=True,blank=True,verbose_name='SSH Password')
    skey = models.TextField(null=True,blank=True,verbose_name='SSH Key')
    sport = models.PositiveIntegerField(null=True,blank=True,verbose_name='SSH Port',default=22)
    cstrng = models.CharField(max_length=30, null=True,blank=True,verbose_name='SNMP Community')
    monitor = models.CharField(max_length=30,null=True,blank=True)
    service = models.TextField(null=True,blank=True,verbose_name='Services')
    process = models.TextField(null=True,blank=True,verbose_name='Processes')
    drive = models.TextField(null=True,blank=True, verbose_name='Drives/Mount Points')
    # sservice = models.TextField(null=True,blank=True,verbose_name='Linux Services')
    # sprocess = models.TextField(null=True,blank=True,verbose_name='Linux Processes')
    os = models.CharField(max_length=100,null=True,blank=True,verbose_name='Operating System')
    board = models.CharField(max_length=50, null=True,blank=True,verbose_name='Motherboard')
    product = models.CharField(max_length=50, null=True,blank=True, verbose_name='Model')
    cpu = models.CharField(max_length=50, null=True,blank=True,verbose_name='CPU')
    ram = models.CharField(max_length=50, null=True,blank=True,verbose_name='RAM')
    # socket = models.CharField(max_length=30, null=True,blank=True)
    active = models.BooleanField(default=True)
    jobid = models.CharField(max_length=32,null=True,blank=True,verbose_name="Job ID")

    def __str__(self):
        return self.name
# Create your models here.

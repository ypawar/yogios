from datetime import datetime
import time
import os

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor


def tick():
    print('Tick! The time is: %s' % datetime.now())


def printName(*name):
    print "Hi",name


if __name__ == '__main__':
    executors = {
        'default': ThreadPoolExecutor(20),
        # 'default': ProcessPoolExecutor(max_workers=5)
    }
    scheduler = BackgroundScheduler(executors=executors)
    # scheduler.add_executor('processpool')
    tjob = scheduler.add_job(tick,'interval', seconds=1)
    pjob = scheduler.add_job(printName,args=['yogesh','pawar'],trigger='interval', seconds=3)
    scheduler.start()
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))
    # scheduler.print_jobs()
    removed = False
    try:
        # This is here to simulate application activity (which keeps the main thread alive).
        while True:
            time.sleep(5)
            print "Main Thread"

            # if not removed:
            #     tjob.remove()
            #     removed = True
            #     scheduler.remove_job(pjob.id)
    except (KeyboardInterrupt, SystemExit):
        # Not strictly necessary if daemonic mode is enabled but should be done if possible
        scheduler.shutdown()
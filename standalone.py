import django
import os
import time
import wmi
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "yogios.settings")
django.setup()
from monitoring.models import Server
import pywin
import sys
from  wmicommands import ExecuteWMI as WMI
from datetime import datetime,timedelta
import time
from linuxcommands import SSH
import time
import  spur
import wmi
import pythoncom
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
import threading
def getservice(result):
    services=[]
    for i,line in enumerate(result.split('\n')):
        # Ignore blank lines
        if line:
            s = line.strip().split()[0]
            if s.endswith('.service'):
                if s[-9] != '@':
                    # print i,s[:-8]
                    services.append(s[:-8])
    return services


def checkAlive(ip,user,pswd,protocol):
    try:
        if protocol=="wmi":
            con = WMI(ip,user,pswd)
        elif protocol=="ssh":
            con = SSH(ip,user,pswd)
        if con.gethostname():
            return True,None
    except spur.ssh.ConnectionError as e:
        return False,e.__str__().split('\n')[1].split(':')[1].strip()
    except wmi.x_access_denied:
        # print e
        return False,"Access is denied"
    except wmi.x_wmi as e:
        print e
        return False,"The RPC server is unavailable."

lock = threading.Lock()


def printAlive(ip,user,pswd,protocol):
    result = checkAlive(ip,user,pswd,protocol)
    with lock:
        print datetime.now(),ip, result

if __name__=='__main__':
    # o = Server.objects.last()
    # print o.wservice,type(o.wservice)
    # print o.wservice.split(',')
    # Server.objects.first().delete()
    # obj = Server(name='testserver5',IP='192.168.0.109',password='yogi@123')
    # obj.save()
    # while True:
    #
    #     time.sleep(1)
    # pass
    # try:
    #     # c = wmi.WMI("192.168.0.109", user='yogesh', password="yogi@123")
    #      c = wmi.WMI()
    # except wmi.x_access_denied, err:
    #     print("Access Denied")
    #     print err
    #     sys.exit()
    # #Get OS Name
    # for os in c.Win32_OperatingSystem():
    #     print os.caption
    # obj = WMI('192.168.0.108','yogesh.pawar','yogi@123')
    obj = WMI()
    # print obj.getservices()

    # print obj.getos()
    # print ' '.join(obj.getcpu().values())
    # obj.getmemory()
    # print obj.getboard()['manufacturer'], obj.getboard()['product']
    # disks= obj.getdisk()
    # for disk in disks:
    #     size = float(disk['size'])
    #     free = float(disk['free'])
    #     used = (size-free)/size*100
    #     used = "{} {:.2f} % Used".format(disk['drive'],used)
    #     print used
    # bt,ct = obj.getuptime().values()
    # print bt
    # bt = float(bt.split('+')[0])#+float(bt.split('+')[1])
    # ct = float(ct.split('+')[0]) #+ float(ct.split('+')[1])
    # # bt = int(bt)
    # print 'bt',bt
    # print 'ct',ct
    # print time.time()
    # print obj.getcpustat()
    # print obj.getmemorystat()

    # sh = ssh('192.168.0.108','yogi','yogi@123')
    # print sh.getmemorystat()
    # utime = sh.getuptime()
    # print timedelta(seconds=float(utime))
    # print sh.getcpu()
    # print sh.getcpustat(),'%'
    pass
    # print obj.getuptime()
    # con = SSH('192.168.0.106','yogi','yogi@123')
    # print con.getos()
    # print con.getcpu()
    # print con.getmemory(),'MB'
    # print con.getboard()
    # print con.getuptime()
    # print con.gethostname()
    # print con.getdrives()
    # print con.getuptime()
    # import sys
    # print sys.platform
    # import spur
    #
    # shell = spur.SshShell(hostname="192.168.0.108", username="yogi", password="yogi@123",
    #                       missing_host_key=spur.ssh.MissingHostKey.accept, shell_type=spur.ssh.ShellTypes.minimal)
    # result = shell.run(["systemctl", "list-unit-files", "--type=service","--plain"]).output
    # s =  getservice(result)
    # for service in s:
    #     # shell = spur.SshShell(hostname="192.168.0.108", username="yogi", password="yogi@123",
    #     #                       missing_host_key=spur.ssh.MissingHostKey.accept, shell_type=spur.ssh.ShellTypes.minimal)
    #     try:
    #         result = shell.run(["systemctl", "status",service],allow_error=False).output
    #         # print result
    #         # lines = result.split('\n')
    #         # for line in lines:
    #         #     if line:
    #         #         line = line.split()
    #         #         # print line
    #         #         # print line[0]
    #         #         if line[0] == 'Active:':
    #         #             print line
    #         #             state = line[1]
    #         #             print service, state
    #     except spur.results.RunProcessError as e:
    #         # print "Error Occurred"
    #         result = e.output
    #     # print result
    #     lines = result.split('\n')
    #     for line in lines:
    #         if line:
    #             line = line.split()
    #             # print line
    #             # print line[0]
    #             if line[0] == 'Active:':
    #                 # print line
    #                 state = ' '.join(line[1:3])
    #                 print service, state
        # finally:

        # time.sleep(1)
    # servers = Server.objects.values_list('id','ip','name','hostname','monitor').filter(active=True)
    scheduler = BackgroundScheduler()
    # pythoncom.CoInitialize()
    servers = Server.objects.filter(active=True)
    # response = {'data':list(servers)}
    # print servers
    for server in servers:
    #     if server.protocol == "wmi":
    #         # print server.ip, server.wuser, server.wpswd
    #         print server.ip, checkAlive(server.ip, server.wuser, server.wpswd, "wmi")
    #     if server.protocol == "ssh":
    #         try :
    #             print server.ip, checkAlive(server.ip, server.suser, server.spswd, "ssh")
    #         except spur.ssh.ConnectionError as e:
    #             print e
        if server.protocol == "wmi":
            j = scheduler.add_job(printAlive,args=[server.ip, server.wuser, server.wpswd, "wmi"],trigger='interval',seconds=30)
            print j.id
        server.jobid = j.id
        server.save()
        if server.protocol == "ssh":
            j = scheduler.add_job(printAlive, args=[server.ip, server.suser, server.spswd, "ssh"], trigger='interval',
                          seconds=30)
            print j.id
            server.jobid = j.id
            server.save()

    scheduler.print_jobs()
    print "Starting checkAlive Jobs"
    scheduler.start()
    removed=False
    while True:
        time.sleep(40)
        if not removed:
            server = Server.objects.get(id=23)
            scheduler.remove_job(server.jobid)
            server.jobid = None
            server.save()
        removed = True
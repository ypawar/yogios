import wmi
from datetime import datetime,timedelta
import socket
import pythoncom
class ExecuteWMI:
    def __init__(self,ip=None,username=None,password=None):
        # if not ip:
        pythoncom.CoInitialize()
        if self.getselfip()==ip:
            self.con = wmi.WMI()
        else:
            self.con = wmi.WMI(ip,user=username,password=password)

    def getos(self):
        for os in self.con.Win32_OperatingSystem():
            return os.Caption.strip()

    def getcpu(self):
        for cpu in self.con.Win32_Processor():
            return {'name':cpu.Name.strip(),'socket':cpu.SocketDesignation}

    def getmemory(self):
        # TODO (Add Page File Support)
        # for mem in self.con.Win32_PhysicalMemory():
            # print float(mem.Capacity)/(2**30),'GB',mem.Manufacturer, mem.PartNumber, mem.Speed
            # return ''.join([str(float(mem.Capacity) / (2 ** 30)), 'GB ', mem.Manufacturer, ' @', str(mem.Speed), 'MHz ',mem.PartNumber])
        for os in self.con.Win32_OperatingSystem():
            return str(int(os.TotalVisibleMemorySize)/(2**10))


    def getboard(self):
        for mbrd in self.con.Win32_BaseBoard():
            return {'manufacturer':mbrd.Manufacturer,'product':mbrd.Product,'SN':mbrd.SerialNumber}

    def getdrives(self):
        drives = []
        for drive in self.con.Win32_LogicalDisk():
            # Add only if mounted / disk drivers return none
            if drive.Size:
                # drives.append({'drive':drive.DeviceID,'free':drive.FreeSpace,'size':drive.Size,'filesystem':drive.FileSystem})
                size = float(drive.Size)
                free = float(drive.FreeSpace)
                used = (size - free) / size * 100.
                used = "{} {:.2f}% Used".format(drive.DeviceID, used)
                drives.append(used)
        return drives

    def getuptime(self):
        for dt in self.con.Win32_OperatingSystem():
            bt = dt.LastBootUpTime
            ct = dt.LocalDateTime
            # Remove + timezone and fraction of seconds
            bt = bt.split('+')[0].split('.')[0]
            ct = ct.split('+')[0].split('.')[0]
            bt = datetime.strptime(bt, '%Y%m%d%H%M%S')
            ct = datetime.strptime(ct, '%Y%m%d%H%M%S')
            ut = ct - bt
            print "BT",bt
            print "CT",ct
            return {'btime': bt, 'ctime': ct, 'utime': ut}

    def getservices(self):
        services=[]
        for service in self.con.Win32_Service():
            # print service.Name
            services.append(service.Name)
        services.sort()
        return services

    def getprocesses(self):
        processes = []
        for process in self.con.Win32_Process():
            if process.Caption not in processes:
                processes.append(process.Caption)
        processes.sort()
        return processes

    def gethostname(self):
        for h in self.con.Win32_ComputerSystem():
            return h.DNSHostName

    def getmemorystat(self):
        for os in self.con.Win32_OperatingSystem():
            return {'total':os.TotalVisibleMemorySize,'free':os.FreePhysicalMemory}

    def getcpustat(self):
        for processor in self.con.Win32_Processor():
            return processor.LoadPercentage

    def getselfip(self):
        return ([l for l in (
            [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [
                [(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in
                 [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])